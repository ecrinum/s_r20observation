## Revue 2.0

#### Retour sur la phase d'observation

&nbsp;

7 et 8 oct. 2019 | Montréal

---



<!-- .element: style="font-size:1.5rem" -->

![logo CRSH](img/crsh.png) <!-- .element: class="logo" style="height:50px; background-color:ghostwhite;padding: 4px" -->
![logo CRCEN](img/LogoENDT10-2016.png) <!-- .element: class="logo" style="height:50px; background-color:ghostwhite;padding: 4px" --> <br/>![CC-BY-SA](http://i.creativecommons.org/l/by-sa/4.0/88x31.png)

===

%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

## Sommaire
<!-- .element: style="width:55%; float:left;" -->

- Méthodologie
- Matériel récolté
- Quelques éléments d'analyse
- Discussion

<!-- .element: style="font-size:0.8em; width:35%; float:left;padding-left:2rem;border-left:1px,solid,white;" -->

===

%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

### Méthodologie
<!-- .element: style="width:55%; float:left;" -->

<!-- .element: style="width:55%; float:left;" -->
- Groupe de travail
- Protocole
  - Argumentaire
  - Récolte de documents
  - Questionnaire
  - Entretiens
  - Suivi d'un article
- (Analyse)

<!-- .element: style="font-size:0.8em; width:35%; float:left;padding-left:2rem;border-left:1px,solid,white;" -->

===


§§§§§SLIDEmoveDown§§§§§

### Groupe de travail

Nicolas Sauret, Servanne Monjour, Juliette De Mayer, Renata Azevedo Moreira, Jeanne Hourez, Marcello Vitali-Rosati

<!-- .element: style="font-size:0.6em" -->

**6 rencontres** entre octobre 2018 et mars 2019 pour :

<!-- .element: style="font-size:0.6em" -->

- identifier nos objectifs
- définir le protocole d'observation
- rédiger un argumentaire
- établir un questionnaire
- proposer des premières analyses
- préparer les entretiens

<!-- .element: style="font-size:0.6em" -->

===

on est reparti du dernier atelier des journées de lancement qui était consacré à cette phase d'observation. Nous avions pu de manière colégiale poser quelques bases pour le protocole, c'est sur cette base que nous avons travaillé.


§§§§§SLIDEmoveDown§§§§§

### Objectifs discutées

- décrire :
  - les acteurs, les pratiques, les protocoles, les outils
  - les rapports entre auteurs et éditeurs
  - le fonctionnement interne des revues du point de vue humain
- identifier :
  - les mécanismes de la légitimation
  - des éléments de conversation
  - ce qui fonctionne, ce qui fonctionne moins
- saisir :
  - le rapport au numérique
  - le rapport à l'éditorialisation
- identifier :
  - les besoins et les modéliser
  - le fossé entre les modèles et les pratiques
- faire exprimer :
  - une vision de l'édition, de la communication scientifique, de la revue
  - le rapport entre le passé et le nouveau

<!-- .element: style="font-size:0.6em" -->

===

- comprendre les inter-relations entre humains, protocoles, outils.
- décrire les rapports entre auteurs et éditeurs: part de la production/structuration (qu'est ce qui est demandé aux auteurs, qu'est ce qui est fait par les auteurs), quelle autorité
- production et structuration
- rapport au numérique :
  - quel rapport les revues entretiennent au numérique
  - qu'est ce que cela a changé pour la revue
  - pour le travail d'édition
  - quel rôle l'éditeur conserve vis à vis de l'éditorialisation:
- rapport à l'éditorialisation : conscience et maîtrise de la présence des articles/revues sur les moteurs de recherches, les agrégateurs, rapport à ces nouveaux modes de diffusions.
- légitimation : où se prenne les décisions, comment,
- conversation :

§§§§§SLIDEmoveDown§§§§§

### Argumentaires


<i class="fa fa-arrow-right"></i> légitimer le processus auprès des équipes, comités, et auteurs

<!-- .element: style="width:50%; float:left; font-size:0.7em; padding-right:1em; margin-top:2em;" -->

![argumentaire](img/argumentaire.jpg)

<!-- .element: style="width:40%; float:left;" -->

===
objectif : convaincre les équipes, les comités, les auteurs égalements.

Ne semble pas avoir rencontré de résistance, nous avons eu très peu de retour à ce stade.

§§§§§SLIDEmoveDown§§§§§

### Questionnaire

![questionnaire](img/questionnaire.png)

<!-- .element: style="width:50%; float:left;" -->

<div style="font-size:0.6em; width:40%; float:left;padding-left:1em;">

<p><i class="fa fa-arrow-right"></i> objectifs</p>
<ul>
<li> préciser le protocole de la revue</li>
<li> identifier "ce qui a de la valeur" pour les revues</li>
<li> identifier les difficultés (complexité, pénibilité, rapports humains)</li>
</ul>
<p><i class="fa fa-arrow-right"></i> étapes</p>
<ul>
<li> étape 1: modélisation d'un protocole standard à partir des chaines éditoriales</li>
<li> étape 2: production du questionnaire</li>
</ul>
</div>

===

Trouver quelques critères : légitimation de la revue (choix d’un auteur, fonctionnement), du texte, qualité du texte, niveaux d’efficacité, rigueur scientifique

Ces éléments de réponse doivent nous permettre d’identifier des moments, des endroits, des noeuds potentiellement problématiques ou importants, que l’on pourra ensuite résoudre, faire bouger avec des expérimentations.

ce qui n'était pas visible dans ce questionnaire, c'est ce que nous cherchions aux interstices des étapes. Derrière la rigidité des étapes, il y avait aussi la volonté de faire réagir sur votre protocole éditorial, c'est-à-dire qu'est ce qui permet, quelle décision permet de passer d'une étape à l'autre, et qu'est ce qui se passe dans ce passage, y a t il une transformation du document ?
Comprendre que chaque étape est un point de contact avec la revue (entre l'éditeur et l'auteur, entre le comité et le directeur de dossier, entre la revue et le diffuseur), or entre ces étapes, on procèdes à des transformations: transformations de status, d'édition, d'autorité.
§§§§§SLIDEmoveDown§§§§§

### Entretiens

<i class="fa fa-arrow-right"></i> **objectifs**

- modéliser l'aspect humain : les tensions, les autorités, les négociations
- évaluer la distance entre le modèle et la pratique

<!-- .element: style="font-size:0.6em" -->


<i class="fa fa-arrow-right"></i> **méthodologie**

- création d'un gabarit
- adaption en fonction des réponses au questionnaire
- entretiens avec les directeur·rice·s de

<!-- .element: style="font-size:0.6em" -->


===

Préparés sur la base des réponses aux questionnaires


%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

### Matériel récolté
<!-- .element: style="width:55%; float:left;" -->

- Revues participantes
- Documents récoltés
- Questionnaire
- Entretiens

<!-- .element: style="font-size:0.8em; width:35%; float:left;padding-left:2rem;border-left:1px,solid,white;" -->

===


§§§§§SLIDEmoveDown§§§§§

### Revues participantes

revue | documents | questionnaire | entretiens
:--|:-:|:-:|--:
Itinéraires|oui|oui|04/04/2019
Mémoires du livre|oui|oui|24/04/2019
Intermédialités|oui|oui|02/05/2019
Études françaises|oui|oui|07/05/2019
Photolittérature|oui|oui|à venir
Sens public|-|oui|-
Cybergéo|non|non|-

<!-- .element: style="font-size:0.8em;" -->


===
§§§§§SLIDEmoveDown§§§§§

### Documents récoltés

![documents récoltés](img/documentsrecoltes.png)

<!-- .element: style="width:55%; float:left;" -->


- tableau général
- rédaction de rapports (non systématiques)
- exploration qualitative

<!-- .element: style="width:40%; float:left; font-size:0.7em;margin-top:2em;" -->

===

- exploration pour préparer les entretiens (courriel, commentaires en marge des articles, évolution d'une proposition de dossier)

§§§§§SLIDEmoveDown§§§§§


![modelisation Itineraires](img/modelisationItineraires.png)

<!-- .element: style="width:40%; float:left;" -->

### Questionnaires

6 questionnaires remplis

<!-- .element: style="font-size:0.7em;" -->

**analyses**

- extraction de points saillants par revue
- modélisation du protocole
- préparation des entretiens

<!-- .element: style="font-size:0.7em;" -->

===
§§§§§SLIDEmoveDown§§§§§

![entretiens](img/entretiens.jpg)

<!-- .element: style="width:30%; float:left;" -->

### Entretiens

- 7,5 heures de vidéos
- transcription complète
- extraction de faits/citations saillantes
- synthèse thématique

<!-- .element: style="font-size:0.7em;" -->

===

EF: 1h42
Intermédialités: 2h00
Itinéraires:2h05
Mémoires du livres:1h50

%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

### Quelques éléments d'analyse
<!-- .element: style="width:55%; float:left;" -->

- Pluralité de modèles
- Constante négociation
- Évaluation et légitimation
- Fracture numérique

<!-- .element: style="font-size:0.8em; width:35%; float:left;padding-left:2rem;border-left:1px,solid,white;" -->

===

Thématiques :

- vision sur le rôle scientifique de la revue dans l’écosystème scientifique, dans la discipline,
- vision sur le rôle éditorial de la revue, du comité. Quelle.s fonction.s ?
- gestes et traces
- procédures
- rapport au numérique
- rapport à la conversation



§§§§§SLIDEmoveDown§§§§§

### Pluralité de modèles


<i class="fa fa-arrow-right"></i>  une diversité de modèles éditoriaux

<!-- .element: style="font-size:0.8em" -->

<i class="fa fa-arrow-right"></i>  une multiplicité de régime épistémologique

<!-- .element: style="font-size:0.8em" -->

===

Malgré les similarités d'outils, de dispositif ou de protocole, nous nous sommes rendus compte qu'il était illusoire de parler d'un modèle unique d'édition pour les sciences humaines. Tout au contraire, les disparités d'un protocole éditorial à l'autre témoigne de singularités très fortes.

Toutes les revues on un comité scientifique, un comité éditorial, des évaluateurs interne ou externe, des directeurs de dossier, toutes les revues procèdent à des évaluations, vérifient les références, elles relisent, éditent, structurent et valident les textes, et tendent toutes vers le meilleur article et la meilleur édition possible..

Ce qui va différencier les revues est la valeur qu'elles vont attribuer à chacun de ces aspects de la production. Et les différences sont manifestes : selon les revues, la rigueur,  l'autorité ou la scientificité ne se jouent pas au même endroit.  
Ainsi le processus de légitimitation n'opère pas de la même façon d'une revue à l'autre.  Au risque de défaire un certain mythe de scientificité et d'objectivité, on peut parler d'une multiplicité de régime épistémologique.


§§§§§SLIDEmoveDown§§§§§

> Ce serait assez artificiel de dire qu’on essaye de maintenir l’anonymat. Ils sont au courant comme le directeur.trice de la revue sait qui a évalué quoi. Ça nous permet aussi de mieux juger, car au fil des années nous comprenons mieux les standards de certains évaluateurs. [Interview&nbsp;1] <!-- .element: style="font-size:0.7em" -->
>
> Moduler un peu la parole d’évangile des évaluateurs c’est à force d’avoir des discussions au comité de rédaction. On se réunit une fois par année, parfois plus, et on fait la liste des problèmes rencontrés dans l’année. L’insatisfaction vis-à-vis des rapports revenait souvent, on voyait que c’était un irritant et on s’est rendu compte qu’il fallait nuancer ça. [Interview&nbsp;1] <!-- .element: style="font-size:0.7em" -->

<!-- .element: style="width:50%; float:left; font-size:0.7em;" -->

### Constante négociation

<i class="fa fa-arrow-right"></i>  Négocier avec un idéal

<!-- .element: style="font-size:0.8em" -->

<i class="fa fa-arrow-right"></i>  tordre le protocole

<!-- .element: style="font-size:0.8em" -->


===

Un autre aspect récurrent que l'on a observé est la négociation constante dans laquelle sont les différents acteurs pendant le processus. Alors, qu'est ce qui est négocié.

C'est en fait une négociation avec le modèle idéal que l'on doit prétendre suivre pour assurer la scientificité de la revue. C'est vraiment un aspect marquant des entretiens. Les documents génériques récoltés ou encore les questionnaires ont plutôt fait ressortir une sorte de modèle idéal, ou modèle à suivre, un protocole et un workflow relativement précis, nous permettant de localiser où étaient prises les décisions.

Mais les entretiens qui ont suivi ont montré qu'à chaque étape du proessus, la réalité reprenait le dessus, et les décisions pour sélectionner, refuser ou en fait pour _autoriser_ un texte étaient soumises à une série de discussions susceptibles de tordre plus ou moins le protocole déclaré.

On peut affirmer je crois qu'en sciences humaines, les règles sont faites pour être transgressées, mais ce n'est pas une surprise. Qui peut croire que les humanités se laisseraient si facilement modéliser et quantifier..? C'est une blague, ou une colle, pour les digital humanists ici présent. Mais en fait il en est de même pour les conditions d'émergence de la pensée, et de sa mise en conversation avec d'autres pensées. Cette pensée et cette conversation ne peut pas fonctionner dans le carcan rigide d'un protocole objectivant.

Et cela vient justifier notre recherche en quelque sorte, une fois que l'on assume le fait que les humanités élaborent des connaissances dans une dynamique conversationnelle, et qu'elles requièrent de ce fait davantage de pensée critique que de protocoles de recherche.

§§§§§SLIDEmoveDown§§§§§

### Interne / externe

<i class="fa fa-arrow-right"></i>  auteurs

<!-- .element: style="font-size:0.8em" -->

<i class="fa fa-arrow-right"></i>  évaluateurs

<!-- .element: style="font-size:0.8em" -->

<i class="fa fa-arrow-right"></i>  directeurs de dossier

<!-- .element: style="font-size:0.8em" -->

===

Il y a une tension particulière entre ce qui est considéré comme interne et ce qui est considéré comme externe, et cette tension est très parlante.

Quelles relations entretiennent les individus impliqués dans le quotidien de la revue  avec les directeurs de dossier, avec les évaluateurs, avec les auteurs ? Les entretiens montrent que ces derniers en effet ne sont pas intégré dans le noyau familial de la revue.

Cette tension se manifeste à différentes étapes du processus éditorial, et de manière différente selon les revues.

D'une revue à l'autre, les experts évaluateurs sont par exemple 1) des "amis", connaissant la politique éditoriale de la revue et donc au fait de ce que la revue attend (de l'auteur et de l'évaluateur), 2) ce peut être parfois des auteurs ayant déjà publié un texte dans la revue, ou pour d'autre 3) des étrangers, sans aucune relation antérieure avec la revue, comme c'est le cas pour Intermédialités.

[Axes sur les reviewers]

On percoit parfois la revue comme une communauté, avec son comité scientifique, son équipe éditoriale, ses lecteurs, ses auteurs réguliers, ses évaluateurs, directeurs de dossier, pourtant cette communauté idéalisée se scinde dans la réalité avec d'un côté la famille interne, et de l'autre le monde extérieur. Nos entretiens avec les directrices et secrétaires de rédaction ont montré que cette distinction est maintenue de manière délibérée, et justifiée sur l'idée selon laquelle réduire la porosité entre la revue et l'extérieur améliorera la scientificité de la revue.

En particulier, dans le cas des directeurs·rices de dossier: à quel point sont ils ou elles impliquées dans les processus d'évaluation et de décision, quel est leur degré d'autonomie, leur degré de collaboration avec le comité. Leur place en dit long sur cette porosité interne/externe. On pourrait presque définir des modèles d'édition à partir de ce degré de porosité.

§§§§§SLIDEmoveDown§§§§§

> On joue un rôle de légitimation du dossier, on fait exister des dossiers, on fonctionne encore beaucoup comme ça. Donc on joue un rôle de légitimation de problématiques. Ce faisant on légitime des auteurs, à commencer par les coordonnateurs qui ont imaginé et monté ce dossier, et puis des auteurs d'articles. [Interview&nbsp;2] <!-- .element: style="font-size:0.7em;" -->

<!-- .element: style="width:50%; float:left; font-size:0.7em;" -->

### Évaluation et légitimation

<i class="fa fa-arrow-right"></i>  processus de décision

<!-- .element: style="font-size:0.8em" -->

<i class="fa fa-arrow-right"></i>  autorités

<!-- .element: style="font-size:0.8em" -->


===

Pour illustrer le point précédent, on peut regarder ce qu'on a observé en terme d'évaluation et de légitimation.

Nous nous demandions où se situait l'autorité ? qu'est ce qui la produisait ? qu'est ce qui, dans le protocole éditorial, participait à la légitimation du texte, d'un auteur, ou plus généralement de la revue elle-même ?

Manifestement, à entendre les différentes personnes avec lesquelles on s'est entretenus, la légitimation n'est pas une science exacte.. Elle résulte plutôt d'une multiplicité de décisionnaires, jouant de leur fonction de décision tout au long du processus. Pour chaque revue, ces petites décisions ne sont pas de la même nature, n'ont pas la même valeur, et ne se situent pas nécessairement aux mêmes étapes.

Par exemple, Etudes Francaise se focalise sur la sélection d'une proposition de dossier dans son ensemble, soumise à la revue avec l'ensemble des articles revus et édités. La première étape dans le processus est la sélection du dossier, la décision est prise en interne, et d'après la directrice, cette première décision a d'abord pour objectif de légitimer la problématique adressée par le dossier.

Evidemment, une autre instance de légitimation est directement liée à l'évaluation. Les revues traditionnellement ont une évaluation interne, et une évaluation externe. Mais la légitimité dans chacune de ces évaluations n'est pas du tout la même d'une revue à l'autre, on s'en apercoit lorsque l'on considère comment et par qui sont validés les évaluations, si les évaluations sont transmises aux auteurs ou non, si elles sont filtrées et éditées par le comité pendant le processus.

Par exemple pour la revue Intermédialités, les évaluations externes sont généralement prises au pied de la lettre, et l'anonymat des évaluateurs demeurent tout au long du process. Leur valeur de légitimation est alors très haute. Au contraire, sur d'autres revues, le comité va pouvoir contrebalancer les évaluations externes par des discussions internes en comité (le comité peut alors connaitre ou non l'identité des évaluateurs), par des évaluations internes, elles aussi pouvant être soit en simple aveugle (l'auteur est anonymisé), soit de manière ouverte (l'évaluateur connait le nom de l'auteur). Selon la revue, sera considéré comme décisive l'évaluation interne ou encore, la discussion interne du comité.

La neutralisation ou la négociation d'une évaluation externe peut aussi faire intervenir l'opinion du directeur ou directrice de dossier, ce qui complexifie encore le processus de décision, et on le voit, en ouvrant ce processus à davantage d'acteur, l'évaluation externe pert progressivement de sa valeur légitimante.

Ainsi, l'autorité légitimante n'est pas unique d'une revue à l'autre, malgré le principe de l'évaluation que tout le monde partage.

§§§§§SLIDEmoveDown§§§§§

### Fracture numérique

<i class="fa fa-arrow-right"></i>  élargissement du lectorat

<!-- .element: style="font-size:0.8em" -->

<i class="fa fa-arrow-right"></i>  perte de capital symbolique

<!-- .element: style="font-size:0.8em" -->

<i class="fa fa-arrow-right"></i>  texte et données, déplacement de la valeur&nbsp;?

<!-- .element: style="font-size:0.8em" -->


===

Lors des entretiens une des problématiques soulevées portait sur l'impact du numérique sur l'édition et la revue.

Il en est ressorti un premier résultat: pour la plupart des personnes impliquées, le numérique est d'abord une question de diffusion, et pas grand chose d'autre. Il a beaucoup été question du fait que publier en ligne permettait d'accroitre le lectorat, d'étendre la portée de la revue, même si cette augmentation s'accompagnait d'une perte de contrôle sur l'identité et la provenance des lecteurs. certes les plateformes sont capables de fournir statistiques et des données sur les usages de lectures, mais la diffusion papier, qui passait notamment par les abonnements assurait un contact humain et institutionnel qui s'est perdu avec les plateformes de diffusion.

Cette perte est d'abord une perte de valeur symbolique. Les directeurs de revue ont ce sentiment que du capital symbolique a été perdu en même temps que de ne plus savoir si la Bibliothèque Bodléienne d'Oxford présente la revue dans ses rayonnages, ou si une autre institution a acheté le dernier numéro.

Ce qui nous a frappé c'est qu'à vous entendre, le numérique n'aurait rien changé au travail éditorial. Microsoft Word demeurant l'unique outil d'édition, il semble que le numérique n'amène aucune valeur au contenu ou à la manière de le produire. Ainsi les revues laissent entre les mains des diffuseurs le soin de produire les données qui feront que la revue et les textes existeront en ligne, seront visibles, requêtable.

On le sait bien aujourd'hui, l'accès à un article ou à une revue se fait principalement à travers les moteurs de recherche. Les métadonnées qui caractérisent les articles sont donc les principales portes d'entrée pour y accéder dans l'océan des publications scientifiques.

Les diffuseurs ne s'y trompent pas et prennent ce travail d'étiquettage très au sérieux, pourtant, nous considérons qu'il y a là un manque très problématique dans le travail scientifique des revues, de ne pas contrôler leurs propres données ni la façon dont elles apparaissent sur le web. Il y a un déséquilibre entre la valeur et l'attention apportées au texte d'un article, et celles apportées à la visibilité du texte auprès des moteurs de recherche.

C'est d'ailleurs ce que relevait Elisabeth Nardout Lafarge lorsqu'elle disait que le FRQSC reconnaissait le travail effectué par le diffuseur, et le financait en conséquence, mais ne reconnaissait pas le travail effectué sur le texte, au point de ne plus le financer. Il y a là une double fracture : du côté de l'éditeur qui n'a pas les moyens de maitriser ses données, mais aussi qui ne le souhaite pas, , et de la part du financeur qui considère que les données sont l'apanage du diffuseur.

Et j'y vois là une méconnaissance profonde de ce qu'est le numérique en tant que support d'écriture et de lecture, en tant que média, environnement, milieu d'écriture.

La production du savoir ne s'arrête à l'écriture d'un texte, mais qu'elle continue dans son édition, dans son éditorialisation, dans sa mise à disposition, c'est-à-dire aussi dans les modalités d'appropriation. Et il y va de l'existence des éditeurs d'avoir une vision sur ces modalités et sur cette production.

---

mariage de raison, != choix conscient et enthousiaste

EF: FRQ reconnait le travail du diffuseur (qu'il paie), et non de l'éditeur.
problématique se déplace vers les lecteurs:  est ce que les lecteurs comprennent les diff. de supports ?
On publie pour un lectorat, quest ce que ca change pour lui.

fracture entre un financeur qui ne finance plus que l'aspect diffusion.
notre discours : réintéger

le numérique n'est pas qu'un vecteur, il participe au sens. C'est aussi au lecteur de se former là-dessus.
%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

### Merci !


<i class="fa fa-arrow-right"></i> présentation : [ecrinum.frama.io/s_r20observation/](https://ecrinum.frama.io/s_r20observation/)

<!-- .element: style="font-size:0.7em;" -->


---
![logo CRCEN](img/LogoENDT10-2016.png) <!-- .element: class="logo" style="width:25%; background-color:ghostwhite;padding: 7px" -->
![CC-BY-SA](http://i.creativecommons.org/l/by-sa/4.0/88x31.png)

===

.slide: data-background-image="img/" data-background-size="cover"
.slide: class="hover"

<!-- .element: style="font-size:1.7rem" -->

<!-- .element: style="font-size:1.7rem; text-align:left; padding-left:1rem;" -->

pour baliser en inline
- yaml <!-- .element: style="color:Darkorange;" -->


pour faire deux colonnes
<!-- .element: style="font-size:0.6em; width:55%; float:left;" -->
<!-- .element: style="font-size:0.6em; width:35%; float:left;padding-left:2rem;border:1px,solid,white;" -->


EF:
- la question du lectorat > focalisation sur le contenu et non sur le support.
- Procès Verbal

Intermédialités

Mémoires
